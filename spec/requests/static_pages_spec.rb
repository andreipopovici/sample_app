require 'spec_helper'


describe "Static Pages" do

  subject {page}

  shared_examples_for "all static pages" do
    it {should have_selector('h1', text: heading)}
    it {should have_selector('title', text: full_title(page_title))}
  end

  describe "Home Page" do
    before { visit root_path }
    let(:heading) {'Sample App'}
    let(:page_title) {''}

    describe "for signed in users" do
      let(:user) { FactoryGirl.create(:user) }
      before do
        FactoryGirl.create(:micropost, user: user, content: "test 1")
        FactoryGirl.create(:micropost, user: user, content: "test 2")
        sign_in user
        visit root_path
      end

      it "should render the user's feed" do
        user.feed.each do |item|
          page.should have_selector("li##{item.id}", text: item.content)
        end
      end
    end

    describe "follower/following count" do
      let(:user) { FactoryGirl.create(:user) }
      let(:other_user) { FactoryGirl.create(:user) }
      before do
        sign_in user
        other_user.follow!(user)
        visit root_path
      end

      it { should have_link("0 following", href: following_user_path(user)) }
      it { should have_link("1 followers", href: followers_user_path(user)) }
    end
  end
  
  describe "Help Page" do
    before { visit help_path }
     let(:heading) {'Help Page'}
     let(:page_title) {'Help'}
  end
  
  describe "About Page" do
    before { visit about_path}
     let(:heading) {'About Page'}
     let(:page_title) {'About'}
  end
  
  describe "Contact" do
    before { visit contact_path }
     let(:heading) {'Contact Page'}
     let(:page_title) {'Contact'}
  end

  it "should have the right links on the layout" do
    visit root_path
    click_link "About"
    page.should have_selector 'title', text: full_title("About")
    click_link "Help"
    page.should have_selector 'title', text: full_title("Help")
    click_link "Contact"
    page.should have_selector 'title', text: full_title("Contact")
    click_link "Home"
    click_link "Sign up now!"
    page.should have_selector 'title', text: full_title('')
    click_link "sample app"
    page.should have_selector 'title', text: full_title('')
  end
end
