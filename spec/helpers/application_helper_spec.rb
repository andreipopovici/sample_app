require 'spec_helper'

describe ApplicationHelper do
  describe "full_title" do
    it "should include page title" do
      full_title("test").should =~ /test/
    end

    it "should include base title" do
      full_title("test").should =~ /^Ruby on Rails Tutorial Sample App/
    end
  end

  it "should not include bar for home page" do
    full_title("").should_not =~ /\|/
  end
end
