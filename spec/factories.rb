FactoryGirl.define do
  factory :user do
    sequence(:name) {|n| "Robot #{n}"}
    sequence(:email) {|n| "test-#{n}@t.com"}
    password  "foobar"
    password_confirmation "foobar"

    factory :admin do
      admin true
    end
  end

  factory :micropost do
    content "Lorem Ipsum"
    user
  end
end
