Feature: Signing in

    Scenario: Unsuccessful Sign In
        Given a user visits the sign in page
        When they submit invalid sign in information
        Then they should see an error message

    Scenario: Successful Sign In
        Given a user visits the sign in page
        And the user has an account
        When the user submits valid sign in information
        Then they should see their profile page
        And they should see a sign out link
