class ApplicationController < ActionController::Base
  protect_from_forgery
  include SessionsHelper # use functions from Sessions helpers in controllers, not just views
end
